# Parallel computing

Files that illustrate how to run a model/function for each entry of a parameter sheet. Additional information can be found in Cari's awesome CHPC bash script (See Carl's version). 